API ENDPOINTS
-------------


/coordinator
<INCOMPLETE DETAILS> {error: true, message: 'Provide all details'}
<SERVER ERROR> {error: true, message: 'Internal server error', try: 'Try again later'}
<INVALID ID> {error: true, message: 'Coordinator validation failed'}
<VALID ID> {error: false, message: 'Coordinator validation success', coordinator}


/login
<INCOMPLETE DETAILS> {error: true, message: 'Provide all details'}
<SERVER ERROR> {error: true, message: 'Internal server error'}
<INVALID USERNAME> {error: true, message: 'Invalid team', try: 'Enter correct details'}
<VALID TEAM> {error: false, message: 'Team authorised', teamcode}
<INVALID PASSWORD> {error: false, message: 'Team not authorised', try: 'Enter correct details'}


/response
<INCOMPLETE DETAILS> {error: true, message: 'Provide all details'}
<ANSWER NOT ACCEPTED> {error: true, message: 'Answer not accepted', try: 'Send valid answers'}
<SERVER ERROR> {error: true, message: 'Internal server error', try: 'Try again later or scan another QR'}
<TEAM VALIDATION> {error: true, message: 'Invalid TeamCode', try: 'Register again'}
<SAVED> {error: false, message: 'Response successfully saved'}
<UPDATION> {error: false, message: 'Response successfully updated'}


/question
<INCOMPLETE DETAILS> {error: true, message: 'Provide all details'}
<SERVER ERROR> {error: true, message: 'Internal server error', try: 'Try again later or scan another QR'}
<TEAM VALIDATION> {error: true, message: 'Invalid TeamCode', try: 'Register again'}
<QR VALIDATION> {error: true, message: 'Invalid QR', try: 'Scan Valid QR'}
<PREVIOUS ENTRIES> {error: true, message: 'Previosly scanned QR', try: 'Scan new QR'}
<FETCH QUESTION> {error: false, message: 'Question fetched', question}


/registration
<INCOMPLETE DETAILS> {error: true, message: 'Provide all details'}
<SERVER ERROR> {error: true, message: 'Internal server error', try: 'Try again later'}
<COORDINATOR VALIDATION> {error: true, message: 'Coordinator validation failed', try: 'Try again later'}
<TEAM REGISTERED> {error: false, message: 'Team registered successfully', username, password, player1, player2}
