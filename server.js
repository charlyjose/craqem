var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');
var randomstring = require('randomstring');
var randomInt = require('random-int');

const mysql = require('mysql');


// Database connection configurations
const mc = mysql.createConnection({
	host: 'XXXXXX',
	user: 'XXXXXX',
	password: 'XXXXXX',
	database: 'XXXXXX',
	port: XXXXXX
});

mc.connect(function (error) {
	if (!error) {
		console.log("Database is connected...\n");
	}
	else {
		console.log("Error connecting database...\n");
	}
});


// Invoke an instance of express application.
var app = express();

// Set our application port
app.set('port', 8080);

// Set morgan to log info about our requests for development use.
app.use(morgan('dev'));

// Initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({ extended: true }));

// Initialize cookie-parser to allow us access the cookies stored in the browser.
app.use(cookieParser());

// Initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
    key: 'user_sid',
    secret: 'XXXXXX',
    resave: false,
    saveUninitialized: false,
    cookie: {
        expires: 1200000 // In 20 minutes session Id expires   //// In 25 minutes 1500000 ms Id expires  //// In 15 minutes 900000 ms Id expires
    }
}));

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
	if (req.cookies.user_sid && !req.session.user) {
		res.clearCookie('user_sid');
	}
	next();
});

// Route for user coordinator
app.get('/', (req, res) => {
    res.redirect('/coordinator');
});

// Route for user registration
app.get('/registration', (req, res) => {
    console.log("\nRequest: " + req.url);
    res.sendFile(__dirname + '/public/registration/registration.html');
});

// Route for user coordinator
app.get('/coordinator', (req, res) =>{
	console.log("\nRequest: " + req.url);
	res.sendFile(__dirname + '/public/coordinator/coordinator.html');
});

// Route for user question
app.get('/question', (req, res) =>{
	console.log("\nRequest: " + req.url);
	res.sendFile(__dirname + '/public/question/question.html');
});

// Route for user response
app.get('/response', (req, res) =>{
	console.log("\nRequest: " + req.url);
	res.sendFile(__dirname + '/public/response/response.html');
});

// Route for user login
app.get('/login', (req, res) => {
	console.log("\nRequest: " + req.url);
	res.sendFile(__dirname + '/public/teamLogin/teamLogin.html');
});




// Fetch question, mapping entry and send question
function fqamesq(TeamCode, QrCode, QCode, res) {
    // Fetch the question
    var sql = 'SELECT * FROM questions WHERE QCode = ?';
    var values = [
        [QCode]
    ];

    mc.query(sql, [values], function (error, results, fields) {
        if (error) {
            console.log("\nERROR OCCURED... <FETCH | QUESTION> [ErrCode 08]");
            return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
        }
        // Fetched the question
        else {
            var question = {
                QCode: results[0].QCode,
                Question: results[0].Question,
                QType: results[0].QType,
                A: results[0].A,
                B: results[0].B,
                C: results[0].C,
                D: results[0].D
            };
            // Add mapping entry
            sql = 'INSERT INTO mapping (TeamCode, QrCode, Qcode) VALUES ?';
            values = [
                [TeamCode, QrCode, QCode]
            ];

            mc.query(sql, [values], function (error, results, fields) {
                if (error) {
                    console.log("\nERROR OCCURED... <INSERTION | mapping> [ErrCode 04]");
                    return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
                }
                // Inserted into mapping
                else {
                    console.log("\nQUERY COMPLETED: <INSERTION | mapping> [SUCCESS]\n", results);
                }
            });
            // Send question
            return res.status(200).send({error: false, message: 'Question fetched', question});
        }
    });
}



//+++++++++++++++++++++++
// COORDINATOR VALIDATION
//+++++++++++++++++++++++

app.post('/coordinator', (req, res) => {
	let ID = req.body.id;

	if (!ID) {
		// ID not provided
		console.log("\nERROR OCCURED... <INCOMPLETE DETAILS> [ErrCode 01]\n");
		return res.status(400).send({error: true, message: 'Provide all details', try: 'Please provide ID'});
	}
	else {
		var sql = 'SELECT * FROM coordinators WHERE id = ?';
		var values = [
			[ID]
		];

		mc.query(sql, [values], function (error, results, fields) {
			if (error) {
				// Coordinator validation error due to database error
				console.log("\nERROR OCCURED... <COORDINATOR VALIDATION | FETCH INFO> [ErrCode 02, 08]\n", error);
				return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later'});
			}
			else if (results.length===0){
				// Coordinator validation error due to empty database results
				console.log("\nERROR OCCURED... <COORDINATOR VALIDATION> [ErrCode 03]\n", error);
				return res.status(401).send({error: true, message: 'Coordinator validation failed'});
			}
			else {
				console.log('\nQUERY COMPLETED: <COORDINATOR VALIDATION> [SUCCESS]\n', results);
				return res.status(200).send({error: false, message: 'Coordinator validation success', coordinator: results[0].Holder});
			}
		});
	}
});



//++++++++++++
// TEAM LOGIN
//++++++++++++

app.post('/login', (req, res) => {
	let username = req.body.username;
	let password = req.body.password;

	// Check if all fields are provided
	if (!username || !password) {
		console.log("\nERROR OCCURED... <INCOMPLETE DETAILS> [ErrCode 01]\n");
		return res.status(400).send({error: true, message: 'Please provide all details'});
	}
	// check for valid username and password
	else {
		var sql = 'SELECT * FROM credentials WHERE Username = ?';
		var values = [
			[username]
		];

		mc.query(sql, [values], function (error, results, fields) {
			if (error) {
				console.log("\nERROR OCCURED... <LOGIN VALIDATION | FETCH INFO> [ErrCode 02, 08]");
	            return res.status(500).send({error: true, message: 'Internal server error'});
			}
			else if (results.length==0) {
				console.log("\nERROR OCCURED... <LOGIN VALIDATION | INVALID Username> [ErrCode 03]");
                return res.status(401).send({error: true, message: 'Invalid team', try: 'Enter correct details'});
			}
			// Details fetched. Validate team
			else {
				// Valid team
				if (results[0].Password===password) {
					return res.status(200).send({error: false, message: 'Team authorised', teamcode: results[0].TeamCode});
				}
				// Invalid team
				else {
					return res.status(400).send({error: false, message: 'Team not authorised', try: 'Enter correct details'});
				}
			}
		});
	}
});



//++++++++++
// RESPONSE
//++++++++++

app.post('/response', (req, res) => {
	let TeamCode = req.body.TeamCode.toString();
	let QCode = req.body.QCode.toString();
	let Answer = req.body.Answer.toString();

	// Check if all fields are provided
	if (!TeamCode || !QCode || !Answer) {
		console.log("\nERROR OCCURED... <INCOMPLETE DETAILS> [ErrCode 01]\n");
		return res.status(400).send({error: true, message: 'Please provide all details'});
	}
	// Check for valid Answers
	else if (Answer!="A" && Answer!="B" && Answer!="C" && Answer!="D" && Answer!="NULL") {
		return res.status(400).send({error: true, message: 'Invalid answer', try: 'Send valid answers'});
	}
	// Check for valid TeamCode
	else {
		var sql = 'SELECT * FROM credentials WHERE TeamCode = ?';
        var values = [
            [TeamCode]
        ];

		mc.query(sql, [values], function (error, results, fields) {
			if (error) {
				console.log("\nERROR OCCURED... <TEAM VALIDATION | FETCH INFO> [ErrCode 02, 08]");
                return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
			}
			// Invalid TeamCode
            else if (results.length==0) {
                console.log("\nERROR OCCURED... <TEAM VALIDATION | INVALID TeamCode> [ErrCode 03]");
                return res.status(401).send({error: true, message: 'Invalid TeamCode', try: 'Register again'});
            }
			// Valid TeamCode
			else {
                // Check for valid QCode
                sql = 'SELECT QCode FROM questions WHERE QCode = ?';
                values = [
                    [QCode]
                ];

                mc.query(sql, [values], function (error, results, fields) {
                    if(error) {
                        console.log("\nERROR OCCURED... <QUESTION VALIDATION | FETCH INFO> [ErrCode 02, 08]");
                        return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later'});
                    }
                    // Invalid QCode
                    else if (results.length==0) {
                        console.log("\nERROR OCCURED... <QUESTION VALIDATION | INVALID QCode> [ErrCode 03]");
                        return res.status(401).send({error: true, message: 'Invalid QCode', try: 'Try again later'});
                    }
                    // Valid QCode
                    else {
						// Check for any previous entry for QCode by TeamCode
						sql = 'SELECT QCode FROM response WHERE TeamCode = ? AND QCode = ?';
						values = [
							[TeamCode],
							[QCode]
						];

						mc.query(sql, values, function (error, results, fields) {
							if(error) {
		                        console.log("\nERROR OCCURED... <PREVIOUS ENTRIES | TeamCode | QCode  | FETCH INFO> [ErrCode 07, 08]" + error);
		                        return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later'});
		                    }
							// No previous entry for QCode by TeamCode
							else if (results.length==0) {
								// Insert response
								sql = 'INSERT INTO response (TeamCode, QCode, Answer) VALUES ?';
								values = [
									[TeamCode, QCode, Answer]
								];

								mc.query(sql, [values], function (error, results, fields) {
									if(error) {
				                        console.log("\nERROR OCCURED... <INSERTION | response> [ErrCode 04]");
				                        return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later'});
				                    }
									// Inserted into response
									else {
										return res.status(200).send({error: false, message: 'Response successfully saved'});
									}
								});
							}
							// have previous entry for QCode by TeamCode
							else {
								// Update Answer in response
								sql = 'UPDATE response SET Answer = ? WHERE TeamCode = ? AND QCode = ?';
								values = [
									[Answer],
									[TeamCode],
									[QCode]
								];

								mc.query(sql, values, function (error, results, fields) {
									if(error) {
				                        console.log("\nERROR OCCURED... <UPDATION | response> [ErrCode 05]");
				                        return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later'});
				                    }
									// Updated response
									else {
										return res.status(200).send({error: false, message: 'Response successfully updated'});
									}
								});
							}
						});
					}
				});
			}
		});
	}
});



//+++++++++++
// QUESTIONS
//+++++++++++

app.post('/question', (req, res) => {
    let TeamCode = req.body.TeamCode.toString();
    let QrCode = req.body.QrCode.toString();

	// Check if all fields are provided
    if  (!TeamCode || !QrCode) {
		console.log("\nERROR OCCURED... <INCOMPLETE DETAILS> [ErrCode 01]\n");
        return res.status(400).send({error: true, message: 'Please provide all details'});
    }
    // Check for valid TeamCode
    else {
        var sql = 'SELECT * FROM credentials WHERE TeamCode = ?';
        var values = [
            [TeamCode]
        ];

        mc.query(sql, [values], function (error, results, fields) {
            if (error) {
                console.log("\nERROR OCCURED... <TEAM VALIDATION | FETCH INFO> [ErrCode 02, 08]");
                return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
            }
            // Invalid TeamCode
            else if (results.length==0) {
                console.log("\nERROR OCCURED... <TEAM VALIDATION | INVALID TeamCode> [ErrCode 03]");
                return res.status(401).send({error: true, message: 'Invalid TeamCode', try: 'Register again'});
            }
            // Valid TeamCode
            else {
                // Check for valid QrCode
                sql = 'SELECT * FROM qrlist WHERE QrCode = ?';
                values = [
                    [QrCode]
                ];

                mc.query(sql, [values], function (error, results, fields) {
                    if(error) {
                        console.log("\nERROR OCCURED... <QR VALIDATION | FETCH INFO> [ErrCode 02, 08]");
                        return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
                    }
                    // Invalid QrCode
                    else if (results.length==0) {
                        console.log("\nERROR OCCURED... <QR VALIDATION | INVALID QrCode> [ErrCode 03]");
                        return res.status(401).send({error: true, message: 'Invalid QR', try: 'Scan Valid QR'});
                    }
                    // Valid QrCode
                    else {
                        // Check for any previous entry for TeamCode in log
                        sql = 'SELECT * FROM mapping WHERE TeamCode = ?';
                        values = [
                            [TeamCode]
                        ];

                        mc.query(sql, [values], function (error, results, fields) {
                            if (error) {
                                console.log("\nERROR OCCURED... <PREVIOUS ENTRIES | TeamCode | FETCH INFO> [ErrCode 07, 08]");
                                return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
                            }
                            // No previous entries for TeamCode
                            else if (results.length==0){
                                // Get a random QCode
                                var QCode = randomInt(100, 110).toString();
                                // Fetch the question
                                // Add mapping entry
                                // Send question
                                fqamesq(TeamCode, QrCode, QCode, res);
                            }
                            // Previous entries for TeamCode is present
                            else {
                                // Check if QrCode is unused by TeamCode
                                sql = 'SELECT * FROM mapping WHERE TeamCode = ?';
                                values = [
                                    [TeamCode]
                                ];

                                mc.query(sql, [values], function (error, results, fields) {
                                    if (error) {
                                        console.log("\nERROR OCCURED... <FETCH UNUSED | QrCode  | FETCH INFO> [ErrCode 08]");
                                        return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
                                    }
                                    else {
                                        var duplicate = 0;
                                        var i = 0;
                                        var TeamRecordLength = results.length;
                                        while (duplicate==0 && i<TeamRecordLength) {
                                            if (results[i++].QrCode==QrCode) {
                                                duplicate = 1;
                                                break;
                                            }
                                            else {
                                                duplicate = 0;
                                            }
                                        }
                                        // Previously used QrCode
                                        if (duplicate==1) {
                                            console.log("\nERROR OCCURED... <PREVIOUS ENTRIES | QrCode> [ErrCode 07]");
                                            return res.status(401).send({error: true, message: 'Previosly scanned QR', try: 'Scan new QR'});
                                        }
                                        // Peviously unused QrCode by TeamCode
                                        else {
                                            // Get unused QCode
                                            sql = 'SELECT QCode FROM mapping WHERE TeamCode = ?';
                                            values = [
                                                [TeamCode]
                                            ];

                                            mc.query(sql, [values], function (error, results, fields) {
                                                if (error) {
                                                    console.log("\nERROR OCCURED... <FETCH UNUSED | QrCode | FETCH INFO> [ErrCode 08]");
                                                    return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later or scan another QR'});
                                                }
                                                // Get unused QCode by TeamCode
                                                else {
                                                    var duplicate = 1;
                                                    var QCode;
                                                    var i = 0;
                                                    var TeamRecordLength = results.length;
                                                    // Find new QCode
                                                    while (duplicate==1 && i<TeamRecordLength) {
                                                        QCode = randomInt(100, 110).toString();
                                                        // Check for duplicate QCode
                                                        if (results[i++].QCode!=QCode) {
                                                            duplicate = 0;
                                                            break;
                                                        }
                                                        else {
                                                            duplicate = 1;
                                                        }
                                                    }
                                                    // Fetch the question
                                                    // Add mapping entry
                                                    // Send question
                                                    fqamesq(TeamCode, QrCode, QCode, res);
                                                }
                                            });
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            }
         });
    }
});



//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// REGISTRATION +++ ADD TEAMS +++ Teams added from coordinator's app
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

app.post('/registration', (req, res) => {
	let Name1 = req.body.name1;
	let Sem1 = req.body.sem1;
	let Dept1 = req.body.dept1;
	let College1 = req.body.college1;
	let Email1 = req.body.email1;
	let Phone1 = req.body.phone1;

	let Name2 = req.body.name2;
	let Sem2 = req.body.sem2;
	let Dept2 = req.body.dept2;
	let College2 = req.body.college2;
	let Email2 = req.body.email2;
	let Phone2 = req.body.phone2;

	let CreatedBy = req.body.CreatedBy;

    // Validating attributes
    if (!Name1 || !Sem1 || !Dept1 || !College1 || !Email1 || !Phone1 || !Name2 || !Sem2 || !Dept2 || !College2 || !Email2 || !Phone2 || !CreatedBy) {
		// Some details are not provided
		console.log("\nERROR OCCURED... <INCOMPLETE DETAILS> [ErrCode 01]\n");
        return res.status(400).send({error: true, message: 'Please provide all details'});
    }

	// Validating coordinator
	var sql = 'SELECT Coordinator FROM coordinators WHERE ID = ?'
	var values = [
		[CreatedBy]
	];

	mc.query(sql, [values] ,function (error, results, fields) {
		if (error) {
			console.log("ERROR OCCURED... <COORDINATOR VALIDATION | FETCH INFO> [ErrCode 02, 08]\n", error);
			return res.status(500).send({error: true, message: 'Internal server error', try: 'Try again later'});
		}
		//Invalid coordinator
		else if (results.length==0){
			// Coordinator validation error due to empty database results
			console.log("\nERROR OCCURED... <COORDINATOR VALIDATION | INVALID ID> [ErrCode 03]\n", error);
			return res.status(401).send({error: true, message: 'Coordinator validation failed', try: 'Try again later'});
		}
		// Valid coordinator
		else {
			// creating unique key
			var TeamCode = Date.now().toString();

			var charset1 = Email1+Sem2+Sem2+Email2+CreatedBy;
			var charset2 = CreatedBy+Email1+Phone2+Phone1+Email2;

			// Create a team with username and password and fill credentials table
			var Username = randomstring.generate({
				length: 6,
				charset: charset1
			});
			var Password = randomstring.generate({
				length: 6,
				charset: charset2
			});

			console.log("\n\n" + Username +"\n\n");
			console.log("\n\n" + Password +"\n\n");

			var sql = 'INSERT INTO credentials (TeamCode, Username, Password) VALUES ?';
			var values = [
				[TeamCode, Username, Password]
			];

			// Team registration and credential insertion
			mc.query(sql, [values], function (error, results, fields) {
				if (error) {
					// Team registration failure due to database error
					console.log("ERROR OCCURED... <TEAM REGISTRATION | CREDENTIALS INSERTION> [ErrCode 04]\n", error);
					return res.status(500).send({error: true, message: 'Internal server error', try: 'Team registration failed'});
				}
				else {
					console.log('\nQUERY COMPLETED: <TEAM CREDENTIALS INSERTION> [SUCCESS]\n', results);

					// Saving registration details
					sql = 'INSERT INTO registrations (TeamCode, Name1, Sem1, Dept1, College1, Email1, Phone1, Name2, Sem2, Dept2, College2, Email2, Phone2, CreatedBy) VALUES ?';
					values = [
						[TeamCode, Name1, Sem1, Dept1, College1, Email1, Phone1, Name2, Sem2, Dept2, College2, Email2, Phone2, CreatedBy]
					];

					mc.query(sql, [values], function (error, results, fields) {
						if (error) {
							// Team registration failure due to database error
							console.log("ERROR OCCURED... <TEAM REGISTRATION | REGISTRATION INSERTION> [ErrCode 04]\n", error);
							return res.status(500).send({error: true, message: 'Internal server error', try: 'Team registration failed'});

							sql = 'DELETE FROM credentials WHERE TeamCode = ?';
							values = [
								[TeamCode]
							];

							mc.query(sql, [values], function (error, results, fields) {
								if (error) {
									// Deleted credentials entry
									console.log("ERROR OCCURED... <CREDENTIALS DELETE> [ErrCode 06]\n", error);
									return res.status(500).send({error: true, message: 'Internal server error', try: 'Team registration failed'});
								}
								else {
									// Deleted credentials entry
									console.log('\nQUERY COMPLETED: <CREDENTIALS DELETE> [SUCCESS]\n', results);
								}
							});
						}
						else {
							console.log('\nQUERY COMPLETED: <TEAM REGISTRATION> [SUCCESS]\n', results);

							// Send saved info
							sql = 'SELECT * from registrations where TeamCode = ? ';
							values = [
								[TeamCode]
							];
							mc.query(sql, [values], function (error, results, fields) {
								if (error) {
									// Team registered but database view error
									console.log("ERROR OCCURED... <TEAM REGISTRATION | FETCH INFO> [ErrCode 08]\n", error);
								}
								else {
									console.log("\nResults: " ,results);

									var player1 = {
										Name: results[0].Name1,
										Semester: results[0].Sem1,
										Department:results[0].Dept1,
										College: results[0].College1,
										Email: results[0].Email1,
										Phone: results[0].Phone1
									};

									var player2 = {
										Name: results[0].Name2,
										Semester: results[0].Sem2,
										Department:results[0].Dept2,
										College: results[0].College2,
										Email: results[0].Email2,
										Phone: results[0].Phone2
									};
									return res.status(200).send({error: false, message: 'Team registered successfully', username: Username, password: Password, player1, player2});
								}
							});
						}
					});
				}
			});
		}
	});
});


// Route for handling 404 requests
app.use(function (req, res, next) {
   res.status(404).sendFile(__dirname + '/public/error/404error.html');
});

// Start the express server
app.listen(app.get('port'), () => console.log(`App started on port ${app.get('port')}`));
