
ERROR CODES
-----------


[ErrCode 01]: <INCOMPLETE DETAILS>
Incomplete details

[ErrCode 02]: <VALIDATION>
Validation error due to database error/server error

[ErrCode 03]: <VALIDATION>
Validation failed, no such database entry

[ErrCode 04]: <INSERTION>
Team registration failure due to database error/server error

[ErrCode 05]: <UPDATION>
Cannot update. Error due to database error/server error

[ErrCode 06]: <DELETE>
Cannot delete. Error due to database error/server error

[ErrCode 07]: <PREVIOUS ENTRIES>
Cannot check for previous entries. Error due to database error/server error

[ErrCode 08]: <FETCH>
Cannot fetch team details. Error due to database error/server error
